## But is it worth it?

Note:
Finally - is using Kotlin worth it?

+++

## In my opinion - yes!

+++

## Android can't into Java8

+++

## Kotlin: Native

+++

## Being a polyglot is a good thing

+++

## And most importantly Kotlin is...

+++

## `fun`

---

### Thank you!

@fa[twitter gp-contact]() @leniwabula

@fa[github gp-contact]() lazybun

@fa[gitlab gp-contact]() lazybun

@fa[envelope gp-contact]() krzysztof@piszkod.pl